"""
This python program starts with the import of the special libraries (a set of functions)

>>> import mysql.connector
>>> from mysql.connector import Error

We are going to use the mysql.connector to connect to a MysSQL (or MariaDB) relational database
This connector hast to be installed with: pip install mysql-connector-python
We also connnect to the Error part of this connector because we want to detect and handle errors
at the moment we use mysql database functions

"""

import mysql.connector
from mysql.connector import Error


def connect_db(db, user, password):
    #  This a standard function to connect to the MySQL database

    try:                                                                            #the use of try: python tries to forfill this part of the program
        if (len(db) > 0):
            connection = mysql.connector.connect(host="localhost", database=db, user=user, password=password)
        else:
            connection = mysql.connector.connect(host="localhost", user=user, password=password)
        if connection.is_connected():
            db_info = connection.get_server_info()
            print("Connect to MySQL Server version ", db_info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record[0])
            return connection
    except Error as e:                                                              # if it doesn't work (error) execute this part of the program
        print("Error while connecting to MySQL", e)


def store_data(connection, table, data):
    """

      This is a function to store three columns data in a database, table smi_example

      It takes three parameters:
      >>> connection:  the connector to the choosen database
      >>> table: name of the table where the information will be stored
      >>> data: An list of tuples with the information to be stored

      Examples:

      >>> storedata(connection, 'smi_example', (10,11,20))

    """

    print("Data to be stored:", data)
    try:
        # prepare the query to store the data
        s_query = ("INSERT INTO {} (col1, col2, col3) values (%s, %s, %s)".format(table))
        print("Query to used:", s_query)
        cursor = connection.cursor()
        cursor.execute(s_query, data)
        connection.commit()
        cursor.close()
    except Error as e:
        print("Error while storing to MySQL", e)


def retrieve_data(connection, table, start, rows):
    """
      This is a function to retrieve three columns data in a database, table smi_example

      It takes four parameters:
      >>> connection:  the connector to the choosen database
      >>> table: name of the table where the information will be stored
      >>> start: the first record to be retrieved
      >>> rows: the number of rows to be retrieved

      Examples:

      >>> result = retrieve_data(connection, 'smi_example', 1, 10)

    """
    try:                                                                            #the use of try: python tries to forfill this part of the program
        s_query = ("SELECT * FROM {} LIMIT {}, {}".format(table, start, rows))
        print("Query to used:", s_query)
        cursor = connection.cursor(buffered=True)
        cursor.execute(s_query)
        data = cursor.fetchall()
        connection.commit()
        cursor.close()
        return data
    except Error as e:                                                              # if it doesn't work (error) execute this part of the program
        print("Error while retreiving from MySQL", e)
        return -1



""""
Assignment 2-a: Create a database and a database table

See als assignment description.

You have to start with creating the database and the database table.

The SQL query to create a database is:
    create database <databasname>
    
The query to create a database table is:
    First connecting to the right database
        use <databasename>
    Then create the table
        CREATE TABLE table_name (
                        column1 datatype,
                        column2 datatype,
                        column3 datatype,
                           ....
                        );    
        CREATE TABLE ex_table(col1 int, col2 int, col3 int);    
    If we follow the examples from the video:
"""

connection = connect_db('', 'student', 'student12345') # connection to database server without a connection to a 								# database

s_query = ("..........")                            # Savety first! delete the database if it already exists
print("Query to used:", s_query)
cursor = connection.cursor(buffered=True)
cursor.execute(s_query)
connection.commit()
cursor.close()

s_query = ("...........")                           # Complete this query to create a database named "example"
print("Query to used:", s_query)
cursor = connection.cursor(buffered=True)
cursor.execute(s_query)
connection.commit()
cursor.close()

s_query = ("...........")                          # Complete this query to connect to the database named "example"
print("Query to used:", s_query)
cursor = connection.cursor(buffered=True)
cursor.execute(s_query)
connection.commit()
cursor.close()

s_query = (".......................................")   # Complete this query to connect to add a table "extable"
                                                                        # with three columns: col1 int, col2 int, col3 int
print("Query to used:", s_query)
cursor = connection.cursor(buffered=True)
cursor.execute(s_query)
connection.commit()
cursor.close()




