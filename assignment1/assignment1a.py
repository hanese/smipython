# Definition of the triangle dimensions

triangle_spaces = [0,1,2,3]
triangle_bottom = 4

# use of indexing the triangle space: print (triangle_spaces[-1])
# use indexes of the triangle spaces to determine the triangle bottom dimensions
# triangle_bottom = triangle_spaces[-1] + 1

# Draw the triangle


for i in triangle_spaces:
    j = 0
    while j < (triangle_bottom - i):
        print(" ", end = "")
        j = j + 1
    print("/", end = "")

    j = 0
    while j < i:
        print(" ", end = "")
        j = j + 1
    print("|")
print("/", end ="")

# Draw the base of the triangle
j = 0
while j < triangle_bottom:
    print("_", end = "")
    j = j + 1
print("|")
